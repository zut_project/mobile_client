import React from 'react';
import {Button, Image, StyleSheet, Text, View} from 'react-native';


class HomeScreen extends React.Component {
  static navigationOptions = {
    title: 'Roomba Tank Controller',
  };
    render() {
        const {navigate} = this.props.navigation;
        return (
                <View style={styles.container}>
                  <Image source={require('../assets/images/turret.png')} style={styles.image}/>
                    <View style={[{width: "80%", flex: 2}]}>
                        <Button
                            onPress={() => navigate('Joysticks')}
                            title="START"
                            containerViewStyle={{alignSelf: 'stretch'}}
                            color="#1EB7D9"
                        />
                    </View>
                    <View style={styles.text}>
                      <Text style={styles.text} onPress={() => navigate('Gitlab')}>Gitlab</Text>
                    </View>
                </View>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    backgroundColor: '#473080',
  },
  image: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
    height: '50%',
    resizeMode: 'contain',
  },
  text: {
    color: '#ffffff',
    fontSize: 15,
    marginBottom: 20,
  },
});

export default HomeScreen;