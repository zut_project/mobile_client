import React from 'react';
import {WebView} from 'react-native';
import index from './joysticks/index.html';

class JoysticksScreen extends React.Component {
    // Nav options can be defined as a function of the screen's props:
    static navigationOptions = ({navigation}) => ({
        title: `Go back`,
    });

    render() {
        return (
            <WebView source={index} onMessage={event => console.log('onMessage', event.nativeEvent.data)}/>
        );
    }
}

export default JoysticksScreen;