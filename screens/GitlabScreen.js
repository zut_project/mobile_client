import React from 'react';
import {WebView} from 'react-native';

class GitlabScreen extends React.Component {
    // Nav options can be defined as a function of the screen's props:
    static navigationOptions = ({navigation}) => ({
        title: `Go back`,
    });

    render() {
        return (
            <WebView source={{uri: 'https://gitlab.com/zut_project/mobile_client'}}/>
        );
    }
}

export default GitlabScreen;