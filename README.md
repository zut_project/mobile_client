# Mobile Client for ZUT Roomba Tank application

React Native client for Roomba Tank application

## Setup

### Development server
```bash
$ git clone https://gitlab.com/zut_project/mobile_client.git
$ cd mobile_client
$ npm install
$ npm start
```

### Running on your device

Install the Expo client app on your iOS or Android phone and connect to the same wireless network as your computer. 

On Android, use the Expo app to scan the QR code from your terminal to open your project. 

On iOS, follow on-screen instructions to get a link.

[More info here](https://facebook.github.io/react-native/docs/getting-started)
