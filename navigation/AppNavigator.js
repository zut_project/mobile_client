import React from 'react';
import { createStackNavigator, createAppContainer} from 'react-navigation';
import HomeScreen from "../screens/HomeScreen";
import JoysticksScreen from "../screens/JoysticksScreen";
import GitlabScrren from "../screens/GitlabScreen";

export default createAppContainer(createStackNavigator({
  Home: { screen: HomeScreen },
  Joysticks: { screen: JoysticksScreen },
  Gitlab: { screen: GitlabScrren }
}));